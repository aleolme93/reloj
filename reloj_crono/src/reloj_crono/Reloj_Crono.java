/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reloj_crono;

import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 *
 * @author MCAO
 */
public class Reloj_Crono extends javax.swing.JFrame {

    /**
     * Creates new form Reloj_Crono
     */
    public Reloj_Crono() {
        initComponents();
        initPaneles();
        initTimerCrono();
        initTimerReloj();
    }
    
    //añadimos los paneles
    private void initPaneles(){
        //Rectangle r= new Rectangle(300,200);
        this.setBounds(new Rectangle(300, 250));
        this.add(panelCrono);
        panelCrono.setBounds(0, 0, 300, 200);
        panelCrono.setVisible(true);
        this.add(panelReloj);
        panelReloj.setBounds(0, 0, 300, 200);
        panelReloj.setVisible(false);
        
    }
    
    private void initTimerCrono(){
        int intervalo = 1000; //1000 milisegundos =  1 segundo
        ActionListener escuchador = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //minutos
                int minutos = contadorSegundos/60;
                String sMinutos;
                if(minutos<10){
                    sMinutos = "0" + minutos;
                }else{
                    sMinutos = "" + minutos;
                }
                //Segundos
                int segundos = contadorSegundos % 60;
                String sSegundos;
                if(segundos<10){
                    sSegundos = "0" + segundos;
                }else{
                    sSegundos = "" + segundos;
                }
                //poner el la pantalla minutos y segundos
                lblPantalla.setText(sMinutos + ":" + sSegundos);
                if(lblPantalla.getText().equals(txfAlarma.getText()) && btnAlarma.isSelected()){
                    mostrarMensaje();
                    cronometro.stop();
                }
                contadorSegundos++;
            }
        };
        cronometro=new Timer(intervalo, escuchador);
    }
    
    private void mostrarMensaje(){
        panelCrono.setVisible(true);
        Icon icono = new ImageIcon(getClass().getResource("/img/alarmClock.png"));
        JOptionPane.showMessageDialog(this, "La alarma sonó!!", "Alarma",JOptionPane.INFORMATION_MESSAGE, icono);
    }
    
    private void initTimerReloj(){
        int intervalo = 1000;
        ActionListener escuchador = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Date hora = new Date();
                SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");
                if(rbtnMadrid.isSelected()){
                    //Madrid
                    lblCiudad.setText("Madrid:");
                    formatoHora.setTimeZone(tzMadrid);
                    lblHora.setText(formatoHora.format(hora));
                }else if(rbtnLondres.isSelected()){
                    ///London
                    lblCiudad.setText("Londres:");
                    formatoHora.setTimeZone(tzLondon);
                    lblHora.setText(formatoHora.format(hora));
                }else if(rbtnNuevaYork.isSelected()){
                    ///NUeva York
                    lblCiudad.setText("Nueva York:");
                    formatoHora.setTimeZone(tzNY);
                    lblHora.setText(formatoHora.format(hora));
                }else if(rbtnTokio.isSelected()){
                    //Tokio
                    lblCiudad.setText("Tokio:");
                    formatoHora.setTimeZone(tzTokio);
                    lblHora.setText(formatoHora.format(hora));
                }else if(rbtnBuenosAires.isSelected()){
                    //Buenos Aires
                    lblCiudad.setText("Buenos Aires:");
                    formatoHora.setTimeZone(tzBuenosAires);
                    lblHora.setText(formatoHora.format(hora));
                }else if(rbtnMoscu.isSelected()){
                    //Moscú
                    lblCiudad.setText("Moscú:");
                    formatoHora.setTimeZone(tzMoscu);
                    lblHora.setText(formatoHora.format(hora));
                }else if(rbtnAuckland.isSelected()){
                    //Auckland
                    lblCiudad.setText("Auckland:");
                    formatoHora.setTimeZone(tzAuckland);
                    lblHora.setText(formatoHora.format(hora));
                }
            }
        };
        relojDigital=new Timer(intervalo, escuchador);  
        relojDigital.start(); //arancamos el reloj
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelCrono = new javax.swing.JPanel();
        lblPantalla = new javax.swing.JLabel();
        btnArrancar = new javax.swing.JButton();
        btnParar = new javax.swing.JButton();
        btnContinuar = new javax.swing.JButton();
        btnAlarma = new javax.swing.JToggleButton();
        txfAlarma = new javax.swing.JTextField();
        lblMensaje = new javax.swing.JLabel();
        panelReloj = new javax.swing.JPanel();
        lblCiudad = new javax.swing.JLabel();
        lblHora = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        mnuVer = new javax.swing.JMenu();
        rbtnReloj = new javax.swing.JRadioButtonMenuItem();
        rbtnCrono = new javax.swing.JRadioButtonMenuItem();
        mnuCiudad = new javax.swing.JMenu();
        rbtnMadrid = new javax.swing.JRadioButtonMenuItem();
        rbtnLondres = new javax.swing.JRadioButtonMenuItem();
        rbtnNuevaYork = new javax.swing.JRadioButtonMenuItem();
        rbtnTokio = new javax.swing.JRadioButtonMenuItem();
        rbtnBuenosAires = new javax.swing.JRadioButtonMenuItem();
        rbtnMoscu = new javax.swing.JRadioButtonMenuItem();
        rbtnAuckland = new javax.swing.JRadioButtonMenuItem();

        lblPantalla.setBackground(new java.awt.Color(0, 0, 0));
        lblPantalla.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblPantalla.setForeground(new java.awt.Color(255, 51, 51));
        lblPantalla.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPantalla.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0), 3));
        lblPantalla.setOpaque(true);

        btnArrancar.setText("Arrancar");
        btnArrancar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnArrancarActionPerformed(evt);
            }
        });

        btnParar.setText("Parar");
        btnParar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPararActionPerformed(evt);
            }
        });

        btnContinuar.setText("Continuar");
        btnContinuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnContinuarActionPerformed(evt);
            }
        });

        btnAlarma.setText("Alarma");

        txfAlarma.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfAlarma.setText("00:00");
        txfAlarma.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txfAlarmaKeyReleased(evt);
            }
        });

        lblMensaje.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout panelCronoLayout = new javax.swing.GroupLayout(panelCrono);
        panelCrono.setLayout(panelCronoLayout);
        panelCronoLayout.setHorizontalGroup(
            panelCronoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCronoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCronoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCronoLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lblMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelCronoLayout.createSequentialGroup()
                        .addGroup(panelCronoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblPantalla, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelCronoLayout.createSequentialGroup()
                                .addGroup(panelCronoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txfAlarma)
                                    .addComponent(btnArrancar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panelCronoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnParar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnAlarma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnContinuar)))
                        .addGap(0, 32, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelCronoLayout.setVerticalGroup(
            panelCronoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCronoLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(lblPantalla, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCronoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnArrancar)
                    .addComponent(btnParar)
                    .addComponent(btnContinuar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCronoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAlarma)
                    .addComponent(txfAlarma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        lblCiudad.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lblCiudad.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lblHora.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lblHora.setForeground(new java.awt.Color(255, 0, 0));
        lblHora.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout panelRelojLayout = new javax.swing.GroupLayout(panelReloj);
        panelReloj.setLayout(panelRelojLayout);
        panelRelojLayout.setHorizontalGroup(
            panelRelojLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblHora, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
            .addComponent(lblCiudad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        panelRelojLayout.setVerticalGroup(
            panelRelojLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRelojLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(lblCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblHora, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(58, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        mnuVer.setText("Ver");

        rbtnReloj.setText("Reloj");
        rbtnReloj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnRelojActionPerformed(evt);
            }
        });
        mnuVer.add(rbtnReloj);

        rbtnCrono.setSelected(true);
        rbtnCrono.setText("Crono");
        rbtnCrono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnCronoActionPerformed(evt);
            }
        });
        mnuVer.add(rbtnCrono);

        jMenuBar1.add(mnuVer);

        mnuCiudad.setText("Ciudad");
        mnuCiudad.setEnabled(false);

        rbtnMadrid.setSelected(true);
        rbtnMadrid.setText("Madrid");
        rbtnMadrid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnMadridActionPerformed(evt);
            }
        });
        mnuCiudad.add(rbtnMadrid);

        rbtnLondres.setText("Londres");
        rbtnLondres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnLondresActionPerformed(evt);
            }
        });
        mnuCiudad.add(rbtnLondres);

        rbtnNuevaYork.setText("Nueva York");
        rbtnNuevaYork.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnNuevaYorkActionPerformed(evt);
            }
        });
        mnuCiudad.add(rbtnNuevaYork);

        rbtnTokio.setText("Tokio");
        rbtnTokio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnTokioActionPerformed(evt);
            }
        });
        mnuCiudad.add(rbtnTokio);

        rbtnBuenosAires.setText("Buenos Aires");
        rbtnBuenosAires.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnBuenosAiresActionPerformed(evt);
            }
        });
        mnuCiudad.add(rbtnBuenosAires);

        rbtnMoscu.setText("Moscú");
        rbtnMoscu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnMoscuActionPerformed(evt);
            }
        });
        mnuCiudad.add(rbtnMoscu);

        rbtnAuckland.setText("Auckland");
        rbtnAuckland.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnAucklandActionPerformed(evt);
            }
        });
        mnuCiudad.add(rbtnAuckland);

        jMenuBar1.add(mnuCiudad);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 180, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnArrancarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnArrancarActionPerformed
        btnContinuar.setEnabled(false);
        btnArrancar.setEnabled(false);
        btnParar.setEnabled(true);
        cronometro.start();
        contadorSegundos=0;
    }//GEN-LAST:event_btnArrancarActionPerformed

    private void btnPararActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPararActionPerformed
        btnParar.setEnabled(false);
        btnArrancar.setEnabled(true);
        btnContinuar.setEnabled(true);
        cronometro.stop();
    }//GEN-LAST:event_btnPararActionPerformed

    private void btnContinuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnContinuarActionPerformed
        btnContinuar.setEnabled(false);
        btnArrancar.setEnabled(false);
        btnParar.setEnabled(true);
        cronometro.start();
    }//GEN-LAST:event_btnContinuarActionPerformed

    private void rbtnCronoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnCronoActionPerformed
         if(rbtnCrono.isSelected()){
            mnuCiudad.setEnabled(false);
            rbtnReloj.setSelected(false);
            panelCrono.setVisible(true);
            panelReloj.setVisible(false);
        }else{
            rbtnCrono.setSelected(true);
        }
    }//GEN-LAST:event_rbtnCronoActionPerformed

    private void txfAlarmaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txfAlarmaKeyReleased
        String sAlarma = txfAlarma.getText();
        boolean formatoValido=true;
        if(sAlarma.length()!=5){
            formatoValido=false;
        }else if(Character.isDigit(sAlarma.charAt(0))==false){
            formatoValido=false;
        }else if(Character.isDigit(sAlarma.charAt(1))==false){
            formatoValido=false;
        }else if(sAlarma.charAt(2)!=':'){
            formatoValido=false;
        }else if(Character.isDigit(sAlarma.charAt(3))==false){
            formatoValido=false;
        }else if(Character.isDigit(sAlarma.charAt(4))==false){
            formatoValido=false;
        }else if(sAlarma.charAt(0)>'5'){
            formatoValido=false;
        }else if(sAlarma.charAt(3)>'5'){
            formatoValido=false;
        }
        if(formatoValido){
            lblMensaje.setText("");
        }else{
            lblMensaje.setText("Error, alarma");
        }
    }//GEN-LAST:event_txfAlarmaKeyReleased

    private void rbtnRelojActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnRelojActionPerformed
        if(rbtnReloj.isSelected()){
            mnuCiudad.setEnabled(true);
            rbtnCrono.setSelected(false);
            panelCrono.setVisible(false);
            panelReloj.setVisible(true);
        }else{
            rbtnReloj.setSelected(true);
        }
    }//GEN-LAST:event_rbtnRelojActionPerformed

    private void rbtnMadridActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnMadridActionPerformed
        if(rbtnMadrid.isSelected()){
            rbtnLondres.setSelected(false);
            rbtnNuevaYork.setSelected(false);
            rbtnTokio.setSelected(false);
            rbtnBuenosAires.setSelected(false);
            rbtnMoscu.setSelected(false);
        }else{
            rbtnMadrid.setSelected(true);
        }
    }//GEN-LAST:event_rbtnMadridActionPerformed

    private void rbtnLondresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnLondresActionPerformed
        if(rbtnLondres.isSelected()){
            rbtnMadrid.setSelected(false);
            rbtnNuevaYork.setSelected(false);
            rbtnTokio.setSelected(false);
            rbtnBuenosAires.setSelected(false);
            rbtnMoscu.setSelected(false);
        }else{
            rbtnLondres.setSelected(true);
        }
    }//GEN-LAST:event_rbtnLondresActionPerformed

    private void rbtnNuevaYorkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnNuevaYorkActionPerformed
        if(rbtnNuevaYork.isSelected()){
            rbtnLondres.setSelected(false);
            rbtnMadrid.setSelected(false);
            rbtnTokio.setSelected(false);
            rbtnBuenosAires.setSelected(false);
            rbtnMoscu.setSelected(false);
        }else{
            rbtnNuevaYork.setSelected(true);
        }
    }//GEN-LAST:event_rbtnNuevaYorkActionPerformed

    private void rbtnTokioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnTokioActionPerformed
        if(rbtnTokio.isSelected()){
            rbtnLondres.setSelected(false);
            rbtnMadrid.setSelected(false);
            rbtnNuevaYork.setSelected(false);
            rbtnBuenosAires.setSelected(false);
            rbtnMoscu.setSelected(false);
        }else{
            rbtnTokio.setSelected(true);
        }
    }//GEN-LAST:event_rbtnTokioActionPerformed

    private void rbtnBuenosAiresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnBuenosAiresActionPerformed
        if(rbtnBuenosAires.isSelected()){
            rbtnLondres.setSelected(false);
            rbtnMadrid.setSelected(false);
            rbtnNuevaYork.setSelected(false);
            rbtnTokio.setSelected(false);
            rbtnMoscu.setSelected(false);
        }else{
            rbtnBuenosAires.setSelected(true);
        }
    }//GEN-LAST:event_rbtnBuenosAiresActionPerformed

    private void rbtnMoscuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnMoscuActionPerformed
        if(rbtnMoscu.isSelected()){
            rbtnLondres.setSelected(false);
            rbtnMadrid.setSelected(false);
            rbtnNuevaYork.setSelected(false);
            rbtnTokio.setSelected(false);
            rbtnBuenosAires.setSelected(false);
        }else{
            rbtnMoscu.setSelected(true);
        }
    }//GEN-LAST:event_rbtnMoscuActionPerformed

    private void rbtnAucklandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnAucklandActionPerformed
        if(rbtnAuckland.isSelected()){
            rbtnLondres.setSelected(false);
            rbtnMadrid.setSelected(false);
            rbtnNuevaYork.setSelected(false);
            rbtnTokio.setSelected(false);
            rbtnBuenosAires.setSelected(false);
            rbtnMoscu.setSelected(false);
        }else{
            rbtnAuckland.setSelected(true);
        }
    }//GEN-LAST:event_rbtnAucklandActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Reloj_Crono.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Reloj_Crono.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Reloj_Crono.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Reloj_Crono.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Reloj_Crono().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btnAlarma;
    private javax.swing.JButton btnArrancar;
    private javax.swing.JButton btnContinuar;
    private javax.swing.JButton btnParar;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JLabel lblCiudad;
    private javax.swing.JLabel lblHora;
    private javax.swing.JLabel lblMensaje;
    private javax.swing.JLabel lblPantalla;
    private javax.swing.JMenu mnuCiudad;
    private javax.swing.JMenu mnuVer;
    private javax.swing.JPanel panelCrono;
    private javax.swing.JPanel panelReloj;
    private javax.swing.JRadioButtonMenuItem rbtnAuckland;
    private javax.swing.JRadioButtonMenuItem rbtnBuenosAires;
    private javax.swing.JRadioButtonMenuItem rbtnCrono;
    private javax.swing.JRadioButtonMenuItem rbtnLondres;
    private javax.swing.JRadioButtonMenuItem rbtnMadrid;
    private javax.swing.JRadioButtonMenuItem rbtnMoscu;
    private javax.swing.JRadioButtonMenuItem rbtnNuevaYork;
    private javax.swing.JRadioButtonMenuItem rbtnReloj;
    private javax.swing.JRadioButtonMenuItem rbtnTokio;
    private javax.swing.JTextField txfAlarma;
    // End of variables declaration//GEN-END:variables
    private Timer cronometro;
    private int contadorSegundos;
    private Timer relojDigital;
    private TimeZone tzMadrid = TimeZone.getTimeZone("Europe/Madrid");
    private TimeZone tzNY= TimeZone.getTimeZone("America/New_York");
    private TimeZone tzLondon = TimeZone.getTimeZone("Europe/London");
    private TimeZone tzTokio = TimeZone.getTimeZone("Asia/Tokyo");
    private TimeZone tzBuenosAires = TimeZone.getTimeZone("America/Buenos_Aires");
    private TimeZone tzMoscu = TimeZone.getTimeZone("Europe/Moscow");
    private TimeZone tzAuckland = TimeZone.getTimeZone("Pacific/Auckland");

}
